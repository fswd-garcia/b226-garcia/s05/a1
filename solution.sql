=======================================
	MYSQL- CULMINATING ACTIVITY
=======================================

-- 1. Return the customerName of the customers who are from the Philippines
SELECT customerName FROM customers WHERE country = "Philippines";


-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
SELECT contactLastName, contactFirstName  FROM customers WHERE customerName LIKE "%La Rochelle Gifts%";


-- 3. Return the product name and MSRP of the product named "The Titanic"
SELECT productName, MSRP FROM products WHERE productName LIKE "The Titanic";


-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
SELECT firstName, lastName FROM employees WHERE  email = "jfirrelli@classicmodelcars.com";


-- 5. Return the names of customers who have no registered state
SELECT customerName FROM customers WHERE state is NULL;


-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" && firstName = "Steve";


-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
SELECT customerName, country, creditLimit FROM customers WHERE 
country != "USA" && creditLimit > 3000;

-- 8. Return the customer numbers of orders whose comments contain the string 'DHL'
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";


-- 9. Return the product lines whose text description mentions the phrase 'state of the art'
SELECT productLine FROM products WHERE productDescription LIKE "%state of the art%";


-- 10. Return the countries of customers without duplication
SELECT distinct country FROM customers;


-- 11. Return the statuses of orders without duplication
SELECT distinct status FROM orders;


-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada
SELECT customerName, country FROM customers WHERE country = "USA" || country = "France" || country = "Canada";


-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo
SELECT firstName, lastName, city FROM employees 
	JOIN offices ON offices.officeCode = employees.officeCode;

-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"

SELECT customerName FROM customers
	JOIN employees ON employees.employeeNumber = customers.salesRepEmployeeNumber WHERE firstName = "Leslie" && lastName = "Thompson";

-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"

SELECT productName, customerName FROM customers
	JOIN orders ON customers.customerNumber = orders.customerNumber 
	JOIN orderdetails ON orderdetails.orderNumber = orderdetails.productCode
	JOIN products ON orderdetails.productCode = products.productCode 
WHERE customers.customerName = "Baane Mini Imports";

-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country

SELECT firstName, lastName, employees.lastName, customers.customerName, offices.country FROM employees
	JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
    JOIN offices ON employees.officeCode = offices.officeCode
    WHERE offices.country = customers.country; 
 


-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000

SELECT productName, quantityInStock FROM products
	JOIN productlines ON products.productLine = productlines.productLine
    WHERE productlines.productLine = "planes" && products.quantityInStock < 1000;

-- 18. Show the customer's name with a phone number containing "+81".

SELECT customerName FROM customers WHERE phone LIKE "%+81%";

-- Find all customers from US

SELECT customerName FROM customers WHERE country = "USA";

-- Show the full details of a customer named La Rochelle Gifts.

SELECT *  FROM customers WHERE customerName LIKE "%La Rochelle Gifts%";

=======================================
	MYSQL- STRETCH GOALS
=======================================

-- 1. Return the customer names of customers whose customer names don't have 'a' in them


-- 2. Return the last names and first names of employees being supervised by "Anthony Bow"


-- 3. Return the product name and MSRP of the product with the highest MSRP



-- 4. Return the number of customers in the UK



-- 5. Return the number of products per product line



-- 6. Return the number of customers served by every employee



-- 7. Show the customer's name with the highest credit limit.

